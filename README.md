An effort to create the cheapest static external ip vpn and bastion setup that is easy to deploy and zero maintenance.
![Sorry but I like to keep track of my projects](https://perzik.aapjeisbaas.nl/perzik.perzik?idsite=4&rec=1&action_name=spot-vpn)
### prerequisites
 - VPC with a public subnet
 - public ssh key for management in: `SSM: /publickeys/${username}`
 - master private key: `SSM: /privatekeys/master` 

### How to deploy
This needs some more attention to make it easy, for now this is the procedure:
 - deploy the cloudformation template
 - login with ssh
 - setup OpenVPN server on port 443 udp
 - connect your ovpn client

### How to use
 - Add your local machines ssh keys to `SSM: /publickeys/${username}`
 - Connect your local machines to the OpenVPN server  
 - Add a default hop to the bastion in your .ssh/config
 - make sure to exclude the locations that you want to access directly, for me this is: local lan, the bastion external and vpn ip

```

Host * !192.168.178.* !34.249.252.145 !10.88.7.1
    ProxyCommand ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o 'ForwardAgent yes' 10.88.7.1 'ssh-add -D ; ssh-add .ssh/master && nc %h %p'
    ForwardAgent           yes
    ServerAliveInterval    60
    LogLevel               ERROR
    StrictHostKeyChecking  no
    UserKnownHostsFile     /dev/null

```

### result
 - You can now login over ssh to locations by giving out **1** key and **1** ip.
 - It also creates a security group which you can use to ssh into ec2 instances. 

## Finding latest amzn2 ami

```
aws ec2 describe-images --region eu-west-1 --owners amazon --filters "Name=name,Values=amzn2-ami-minimal-hvm*" "Name=architecture,Values=x86_64" "Name=architecture,Values=x86_64" --query 'sort_by(Images, &CreationDate)[-1].ImageId'
"ami-0aa809e00ff0dfac4"
```